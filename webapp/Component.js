sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"ZITI/LogiMat2020CheckIn/model/models"
], function (UIComponent, Device, models) {
	"use strict";
	var globalModel = new sap.ui.model.json.JSONModel({
		"languages": [{
			"roundFlag": "images/icons8-great-britain-96.png",
			"flag": "images/flag_english.png",
			"label": "English",
			"key": "en"
		}, {
			"roundFlag": "images/icons8-germany-96.png",
			"flag": "images/flag_german.png",
			"label": "Deutsch",
			"key": "de"
		}, {
			"roundFlag": "images/icons8-russian-federation-96.png",
			"flag": "images/flag_russian.png",
			"label": "Pу́сски",
			"key": "ru"
		}, {
			"roundFlag": "images/icons8-poland-96.png",
			"flag": "images/flag_polish.png",
			"label": "Polski",
			"key": "pl"
		}, {
			"roundFlag": "images/icons8-czech-republic-96.png",
			"flag": "images/flag_czech.png",
			"label": "český",
			"key": "cz"
		}, {
			"roundFlag": "images/icons8-spain-96.png",
			"flag": "images/flag_spain.png",
			"label": "España",
			"key": "sp"
		}, {
			"roundFlag": "images/icons8-hungarian-96.png",
			"flag": "images/flag_hungarian.png",
			"label": "Magyar",
			"key": "hun"
		}, {
			"roundFlag": "images/icons8-romanian-96.png",
			"flag": "images/flag_romanian.png",
			"label": "Română",
			"key": "rou"
		}, {
			"roundFlag": "images/icons8-bulgarian-96.png",
			"flag": "images/flag_bulgarian.png",
			"label": "български",
			"key": "bgr"
		}],
		"zoomLevel": [{
			"text": "100%",
			"key": "100%"
		}, {
			"text": "110%",
			"key": "110%"
		}, {
			"text": "120%",
			"key": "120%"
		}, {
			"text": "130%",
			"key": "130%"
		}, {
			"text": "140%",
			"key": "140%"
		}, {
			"text": "150%",
			"key": "150%"
		}, {
			"text": "160%",
			"key": "160%"
		}, {
			"text": "170%",
			"key": "170%"
		}, {
			"text": "180%",
			"key": "180%"
		}, {
			"text": "190%",
			"key": "190%"
		}, {
			"text": "200%",
			"key": "200%"
		}],
		"actualZoom": "100%",
		"actualLanguageImage": "images/icons8-great-britain-96.png",
		"showNextButton": true,
		"step1Visible": true,
		"step2Visible": true,
		"step3Visible": true,
		"step4Visible": true,
		"step5Visible": true,
		"step6Visible": true,
		"step7Visible": true,
		"step8Visible": true,
		"step9Visible": true,
		"enableBackButton": false,
		"loadOnlyOneTime": false,
		"actualStep": 1,
		"showHeader": false
	});

	sap.ui.getCore().setModel(globalModel, "globalVariable");

	return UIComponent.extend("ZITI.LogiMat2020CheckIn.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// set global model
			sap.ui.getCore().setModel(globalModel, "gm");

			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		}
	});
});